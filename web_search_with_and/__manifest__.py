# Copyright 2015 Andrius Preimantas <andrius@versada.lt>
# Copyright 2020 ACSONE SA/NV
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Use AND conditions on omnibar search",
    "version": "2.0.1.0.1",
    "author": "Versada UAB, ACSONE SA/NV, Serincloud, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "category": "web",
    "website": "https://gitlab.com/flectra-community/web",
    "depends": ["web"],
    "data": ["views/assets.xml"],
}
