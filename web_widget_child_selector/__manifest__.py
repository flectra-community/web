# Copyright 2019 Creu Blanca
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Web Widget Child Selector",
    "summary": "Widget used for navigation on hierarchy fields",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "author": "Creu Blanca,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/web",
    "depends": ["web"],
    "data": ["views/assets.xml"],
    "qweb": ["static/src/xml/widget_child_selector.xml"],
}
