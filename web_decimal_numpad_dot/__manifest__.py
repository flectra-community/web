# Copyright 2015 AvanzOSC - Oihane Crucelaegui
# Copyright 2015 Tecnativa - Pedro M. Baeza
# Copyright 2015 Comunitea - Omar Castiñeira Saavedra
# Copyright 2016 Oliver Dony
# Copyright 2017-18 Tecnativa - David Vidal
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Web - Numpad Dot as decimal separator",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "summary": "Allows using numpad dot to enter period decimal separator",
    "depends": ["web"],
    "author": "AvanzOSC, "
    "Comunitea, "
    "Tecnativa, "
    "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/web",
    "category": "Web",
    "data": ["views/web_decimal_numpad_dot.xml"],
    "installable": True,
}
