{
    "name": "Web Remember Tree Column Width",
    "summary": "Remember the tree columns' widths across sessions.",
    "author": "Vauxoo, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/web",
    "license": "LGPL-3",
    "category": "Extra Tools",
    "version": "2.0.1.0.0",
    "maintainers": [
        "frahikLV",
        "luisg123v",
    ],
    "depends": ["web"],
    "data": ["views/assets.xml"],
    "installable": True,
}
