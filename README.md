# Flectra Community / web

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[web_send_message_popup](web_send_message_popup/) | 2.0.1.0.0| Web Send Message as Popup
[web_widget_char_size](web_widget_char_size/) | 2.0.1.0.1| Add size option to Char widget
[web_widget_image_webcam](web_widget_image_webcam/) | 2.0.1.0.1| Allows to take image with WebCam
[web_pwa_oca](web_pwa_oca/) | 2.0.1.0.0| Make Odoo a PWA
[web_notify](web_notify/) | 2.0.1.0.1|         Send notification messages to user
[web_m2x_options_manager](web_m2x_options_manager/) | 2.0.1.1.0| Adds an interface to manage the "Create" and "Create and Edit" options for specific models and fields.
[web_widget_x2many_2d_matrix](web_widget_x2many_2d_matrix/) | 2.0.1.1.1| Show list fields as a matrix
[web_widget_mermaid](web_widget_mermaid/) | 2.0.1.0.0| Render mermaid markdown flowcharts
[web_copy_confirm](web_copy_confirm/) | 2.0.1.0.0| Show confirmation dialogue before copying records
[web_widget_url_advanced](web_widget_url_advanced/) | 2.0.1.0.1| This module extends URL widget for displaying anchors with custom labels.
[web_environment_ribbon](web_environment_ribbon/) | 2.0.1.0.0| Web Environment Ribbon
[web_advanced_search](web_advanced_search/) | 2.0.1.0.1| Easier and more powerful searching tools
[web_tree_image_tooltip](web_tree_image_tooltip/) | 2.0.1.0.0| Show images in tree views via tooltip
[web_widget_dropdown_dynamic](web_widget_dropdown_dynamic/) | 2.0.1.0.0| This module adds support for dynamic dropdown widget
[web_widget_child_selector](web_widget_child_selector/) | 2.0.1.0.1| Widget used for navigation on hierarchy fields
[web_widget_ckeditor](web_widget_ckeditor/) | 2.0.1.0.2| Provides a widget for editing HTML fields using CKEditor
[web_access_rule_buttons](web_access_rule_buttons/) | 2.0.1.0.1| Disable Edit button if access rules prevent this action
[web_refresher](web_refresher/) | 2.0.2.0.0| Web Refresher
[web_disable_export_group](web_disable_export_group/) | 2.0.2.0.0| Web Disable Export Group
[web_widget_plotly_chart](web_widget_plotly_chart/) | 2.0.1.0.0| Allow to draw plotly charts.
[web_m2x_options](web_m2x_options/) | 2.0.1.1.1| web_m2x_options
[web_company_color](web_company_color/) | 2.0.2.0.0| Web Company Color
[web_calendar_slot_duration](web_calendar_slot_duration/) | 2.0.1.0.0| Customizable calendar slot durations
[web_widget_image_download](web_widget_image_download/) | 2.0.1.0.0| Allows to download any image from its widget
[web_timeline](web_timeline/) | 2.0.2.0.2| Interactive visualization chart to show events in time
[web_decimal_numpad_dot](web_decimal_numpad_dot/) | 2.0.1.0.0| Allows using numpad dot to enter period decimal separator
[web_notify_channel_message](web_notify_channel_message/) | 2.0.1.0.0|         Send an instant notification to channel users when a new message is posted
[web_ir_actions_act_window_message](web_ir_actions_act_window_message/) | 2.0.1.0.1| Show a message box to users
[web_view_calendar_list](web_view_calendar_list/) | 2.0.1.0.0|         Show calendars as a List
[web_widget_many2one_simple](web_widget_many2one_simple/) | 2.0.1.0.0| Simple many2one widget
[web_widget_open_tab](web_widget_open_tab/) | 2.0.1.0.0|         Allow to open record from trees on new tab from tree views
[web_field_required_invisible_manager](web_field_required_invisible_manager/) | 2.0.2.1.3| Web Field Required Invisible Readonly Managerr
[web_widget_model_viewer](web_widget_model_viewer/) | 2.0.1.0.0| Easily display interactive 3D models on the web & in AR
[web_widget_numeric_step](web_widget_numeric_step/) | 2.0.1.0.0| Web Widget Numeric Step
[web_sheet_full_width](web_sheet_full_width/) | 2.0.1.0.1| Use the whole available screen width when displaying sheets
[web_dialog_size](web_dialog_size/) | 2.0.1.0.0|         A module that lets the user expand a        dialog box to the full screen width.
[web_widget_mpld3_chart](web_widget_mpld3_chart/) | 2.0.1.0.0| This widget allows to display charts using MPLD3 library.
[web_widget_text_markdown](web_widget_text_markdown/) | 2.0.1.0.0| Widget to text fields that adds markdown support
[web_widget_datepicker_fulloptions](web_widget_datepicker_fulloptions/) | 2.0.1.0.0| Web Widget DatePicker Full Options
[web_calendar_color_field](web_calendar_color_field/) | 2.0.1.0.0| Calendar Color Field
[web_create_write_confirm](web_create_write_confirm/) | 2.0.1.0.0| Confirm/Alert pop-up before saving
[web_no_bubble](web_no_bubble/) | 2.0.1.0.0| Remove the bubbles from the web interface
[web_search_with_and](web_search_with_and/) | 2.0.1.0.1| Use AND conditions on omnibar search
[web_widget_bokeh_chart](web_widget_bokeh_chart/) | 2.0.2.3.1| This widget allows to display charts using Bokeh library.
[web_domain_field](web_domain_field/) | 2.0.1.0.2|         Use computed field as domain
[web_ir_actions_act_multi](web_ir_actions_act_multi/) | 2.0.1.0.1| Enables triggering of more than one action on ActionManager
[web_tree_many2one_clickable](web_tree_many2one_clickable/) | 2.0.1.0.2| Open the linked resource when clicking on their name
[web_remember_tree_column_width](web_remember_tree_column_width/) | 2.0.1.0.0| Remember the tree columns' widths across sessions.
[web_drop_target](web_drop_target/) | 2.0.1.1.1| Allows to drag files into Odoo
[web_listview_range_select](web_listview_range_select/) | 2.0.1.0.0|         Enables selecting a range of records using the shift key    
[web_widget_domain_editor_dialog](web_widget_domain_editor_dialog/) | 2.0.1.0.1| Recovers the Domain Editor Dialog functionality
[support_branding](support_branding/) | 2.0.1.0.1| Adds your branding to an Odoo instance
[web_ir_actions_act_view_reload](web_ir_actions_act_view_reload/) | 2.0.1.0.2| Enables reload of the current view via ActionManager
[web_tree_dynamic_colored_field](web_tree_dynamic_colored_field/) | 2.0.1.0.0| Allows you to dynamically color fields on tree views
[web_switch_context_warning](web_switch_context_warning/) | 2.0.1.0.0| Show a warning if current user, company or database have been switched in another tab or window.
[web_action_conditionable](web_action_conditionable/) | 2.0.1.0.1| web_action_conditionable
[web_responsive](web_responsive/) | 2.0.1.2.2| Responsive web client, community-supported
[web_group_expand](web_group_expand/) | 2.0.1.0.0| Group Expand Buttons


