# © 2017 Onestein (<http://www.onestein.eu>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "List Range Selection",
    "summary": """
        Enables selecting a range of records using the shift key
    """,
    "version": "2.0.1.0.0",
    "category": "Web",
    "author": "Onestein, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/web",
    "license": "AGPL-3",
    "depends": ["web"],
    "data": ["templates/assets.xml"],
    "installable": True,
    "application": False,
}
