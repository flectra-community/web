# Copyright 2020 Tecnativa - Alexandre D. Díaz
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Simple many2one widget",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/web",
    "depends": ["web"],
    "data": ["views/assets.xml"],
    "qweb": ["static/src/xml/many2one_simple.xml"],
    "installable": True,
    "maintainers": ["Tardo"],
}
