# Copyright 2019-2020 Brainbean Apps (https://brainbeanapps.com)
# Copyright 2020 CorporateHub (https://corporatehub.eu)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Dynamic Dropdown Widget",
    "summary": "This module adds support for dynamic dropdown widget",
    "category": "Web",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "CorporateHub, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/web",
    "depends": ["web"],
    "data": ["templates/assets.xml"],
    "installable": True,
}
