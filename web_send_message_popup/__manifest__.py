# Copyright 2014-2021 Camptocamp SA
# License AGPL-3.0 or later (http://gnu.org/licenses/agpl).
{
    "name": "Web Send Message as Popup",
    "version": "2.0.1.0.0",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "maintainer": "Camptocamp",
    "license": "AGPL-3",
    "category": "Hidden",
    "depends": ["web", "mail"],
    "website": "https://gitlab.com/flectra-community/web",
    "data": ["templates/assets.xml"],
}
