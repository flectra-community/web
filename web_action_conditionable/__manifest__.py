# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "web_action_conditionable",
    "version": "2.0.1.0.1",
    "depends": ["base", "web"],
    "data": ["templates/assets.xml"],
    "author": "Cristian Salamea,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/web",
    "license": "AGPL-3",
    "installable": True,
}
