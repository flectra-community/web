# Copyright 2021 Tecnativa - Jairo Llopis
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
{
    "name": "Calendar slot duration",
    "summary": "Customizable calendar slot durations",
    "version": "2.0.1.0.0",
    "development_status": "Production/Stable",
    "category": "Extra Tools",
    "website": "https://gitlab.com/flectra-community/web",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "maintainers": ["Yajo"],
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "depends": ["web"],
    "data": ["templates/assets.xml"],
}
