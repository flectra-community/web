# Copyright 2019 Iván Todorovich
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Calendar Color Field",
    "version": "2.0.1.0.0",
    "category": "Web",
    "website": "https://gitlab.com/flectra-community/web",
    "author": "Iván Todorovich, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": [
        "web",
    ],
    "data": [
        "views/assets.xml",
    ],
}
