# Copyright 2018 Simone Orsi - Camptocamp SA
# License LGPLv3.0 or later (https://www.gnu.org/licenses/lgpl-3.0.en.html).
{
    "name": "Web URL widget advanced",
    "summary": "This module extends URL widget "
    "for displaying anchors with custom labels.",
    "category": "Web",
    "version": "2.0.1.0.1",
    "license": "LGPL-3",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/web",
    "depends": ["web"],
    "data": ["templates/assets.xml"],
    "installable": True,
}
