{
    "name": "Group Expand Buttons",
    "category": "Web",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Flectra SA, "
    "AvanzOSC, "
    "Serv. Tecnol. Avanzados - Pedro M. Baeza, "
    "Therp BV, "
    "Xtendoo, "
    "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/web",
    "depends": ["web"],
    "qweb": ["static/src/xml/expand_buttons.xml"],
    "data": ["views/templates.xml"],
}
